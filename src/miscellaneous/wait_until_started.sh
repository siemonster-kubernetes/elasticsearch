#!/bin/bash

until nc -w 1 -z ${DISCOVERY_SERVICE} 9300; do 
    echo waiting for elasticsearch to come up; sleep 3; 
done
